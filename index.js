/* eslint-disable no-console */
const fs = require('fs');
const fetch = require('node-fetch');
const prettier = require('prettier');
const punycode = require('punycode');
const YAML = require('yaml');
const _ = require('lodash');

const encoding = 'utf8';

(async () => {
  const iana = await fetch('https://data.iana.org/TLD/tlds-alpha-by-domain.txt')
    .then((res) => res.text())
    .then((text) => text.split('\n').filter(
      (tld) => tld && !tld.startsWith('#'),
    ).map((tld) => tld.toLowerCase()));

  const publicSuffix = await fetch('https://publicsuffix.org/list/public_suffix_list.dat')
    .then((res) => res.text())
    .then((text) => text.split('\n').filter(
      (tld) => tld && !tld.startsWith('//'),
    ).map((tld) => tld
      .replace('!', '')
      .replace('*.', '')
      .toLowerCase()));

  const tlds = _.uniq([...iana, ...publicSuffix].map((tld) => punycode.toASCII(tld)).sort());

  fs.writeFileSync(
    'public/tlds.txt',
    tlds.join('\n'),
    encoding,
  );

  fs.writeFileSync(
    'public/tlds.json',
    prettier.format(JSON.stringify(tlds), { parser: 'json' }),
    encoding,
  );

  fs.writeFileSync(
    'public/tlds.yaml',
    prettier.format(YAML.stringify(tlds), { parser: 'yaml' }),
    encoding,
  );
})();

# TLD

Nexylan valid TLDs and suffixes list.

## Usage

### TLDs data file

TLDs are available on `public/tlds.{format}` file.

To see all available formats, go to: https://tld.nexylan.dev/

#### Direct link

```
wget https://tld.nexylan.dev/tlds.{format}
```

#### Dockerfile

```Dockerfile
COPY --from=registry.gitlab.com/nexylan/tld public/tlds.{format} /where/you/want
```

## Development

```
make
```
